global without sharing class CreateTaskBatch implements Database.Batchable<SObject>
{
    public Database.QueryLocator start( Database.BatchableContext bc)
    {
        return Database.getQueryLocator( [SELECT OwnerId,AccountId, Name, CloseDate, StageName FROM Opportunity WHERE StageName = 'Closed Won' ] );

    }
    global void execute( Database.BatchableContext bc , List<Opportunity> lstOpp )
    {
        
        List<Task> lstInsertTask = new List<Task>();
        set<Id> setOppId = new set<Id>();
        set<Id> setOppIdOfTask = new set<Id>();

        for(Opportunity objOpp : lstOpp)
        {
            setOppId.add(objOpp.Id);
        }

        for(Task objTaskInfo : [SELECT Id, OwnerId, Subject, WhatId FROM Task WHERE WhatId IN : setOppId AND Subject = 'Opportunity is closed and won' ])
        {
            setOppIdOfTask.add(objTaskInfo.WhatId);
        }
        
        for(Opportunity objOppo : lstOpp)
        {
            if(!setOppIdOfTask.contains(objOppo.Id))
            {
                Task objTask = new Task();
                objTask.Subject = 'Opportunity is closed and won';
                objTask.Type = 'Email';
                objTask.Status = 'Completed';
                objTask.Priority = 'Normal';
                objTask.OwnerId = objOppo.OwnerId;
                objTask.WhatId = objOppo.Id;
                lstInsertTask.add(objTask);
            }
        }
        if(!lstInsertTask.IsEmpty())
        {
            insert lstInsertTask;
        }
        
    }
    global void finish( Database.BatchableContext bc)
    {
       /* List<Task> lstVerTask = [SELECT Id FROM Task];
        System.debug('//-------- BATCH CLASS -------//');
        System.debug('No of Tasks: ' + lstVerTask.size());*/
        System.debug('Program Working Properly!!!');
    }
}
