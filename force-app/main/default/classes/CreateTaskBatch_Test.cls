@isTest public with sharing class CreateTaskBatch_Test 
{
    @isTest public static void testOnExecute() 
    {
        //List<Opportunity> lstOpp = new List<Opportunity>();      
        List<Opportunity> lstOpp = new List<Opportunity>{ new Opportunity(Name ='New Opportunity: ', CloseDate = System.today(), StageName = 'Closed Won'),
                                                          new Opportunity( Name ='New Person2', CloseDate= System.today(), StageName = 'Prospecting'),
                                                          new Opportunity( Name ='New Person', CloseDate= System.today(), StageName = 'Closed Won') };
        insert lstOpp;

        //Opportunity objOppn = [SELECT Name, AccountId, OwnerId FROM Opportunity WHERE Name ='New Person'];
        Task objTask = new Task( Subject ='Opportunity is closed and won', WhatId = lstOpp[2].Id, OwnerId = lstOpp[2].OwnerId, Priority ='Normal' );
        insert objTask;

        Test.startTest();
        CreateTaskBatch createTb = new CreateTaskBatch();
        Id batchId = Database.executeBatch(createTb);
        Test.stopTest();

        List<Opportunity> lstVerOpp = [SELECT Id FROM Opportunity];
        System.assertEquals(3, lstVerOpp.size());

        List<Task> lstVerTask = [SELECT OwnerId FROM Task ];
        System.assertEquals(2, lstVerTask.size());


    }
}
