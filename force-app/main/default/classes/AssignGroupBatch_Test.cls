@isTest public without sharing class AssignGroupBatch_Test 
{
   @isTest public static void testOnAssignGroupBatchInsert() 
    {
        Profile profUser = [SELECT Id, Name FROM Profile WHERE Name ='Standard User'];
        Profile profAdmin = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
        List<User> lstUser = new List<User>{ new User( LastName = 'Test User One', Email = 'testuser01@gmail.com', alias ='n_user1', Username = 'randomtestuser0011@gmail.com', emailencodingkey='UTF-8', languagelocalekey='en_US', timezonesidkey='Europe/London', localesidkey='en_US', ProfileId = profUser.Id, isActive = True),
                                             new User( LastName = 'Test Administrator One', Email ='testadmin02@gmail.com', alias= 'n_admin1',  Username = 'randomtestadmin0022@gmail.com', emailencodingkey='UTF-8',  languagelocalekey='en_US', localesidkey='en_US', timezonesidkey='Europe/London', ProfileId = profAdmin.Id, isActive = True) };

        insert lstUser;
        System.debug(lstUser);
        System.assertEquals(2, lstUser.size());

        Group grpUser =[SELECT Id, Name FROM Group WHERE Name = 'Users'];
        Group grpAdmin =[SELECT Id, Name FROM Group WHERE Name = 'Admins'];
           

        Test.startTest();
        AssignGroupBatch gB = new AssignGroupBatch();
        Id jobId = Database.executeBatch(gB);
        Test.stopTest();

        List<GroupMember> lstGmVerUser = [SELECT Group.Name, GroupId, UserOrGroupId FROM GroupMember WHERE GroupId =:grpUser.Id ];
        System.debug(lstGmVerUser.size());
        List<GroupMember> lstGmVerAdmin = [SELECT Group.Name, GroupId, UserOrGroupId FROM GroupMember WHERE GroupId =:grpAdmin.Id ];
        System.debug(lstGmVerAdmin.size());
    }
    @isTest public static void testOnAssignGroupBatchDelete() 
    {
        Profile profUser = [SELECT Id, Name FROM Profile WHERE Name ='Standard User'];
        Profile profAdmin = [SELECT Id, Name FROM Profile WHERE Name ='System Administrator'];
        Group grpUser =[SELECT Id, Name FROM Group WHERE Name = 'Users'];
        Group grpAdmin =[SELECT Id, Name FROM Group WHERE Name = 'Admins'];

        User lstUserDel = new User( LastName = 'Test Administrator Two', Email ='testadmin021@gmail.com', alias= 'n_admin2',  Username = 'randomtestadmin00222@gmail.com', emailencodingkey='UTF-8',  languagelocalekey='en_US', localesidkey='en_US', timezonesidkey='Europe/London', ProfileId = profAdmin.Id, isActive = True);
        insert lstUserDel;
        GroupMember newGroupMemberUser = new GroupMember(GroupId = grpUser.Id, UserOrGroupId = lstUserDel.Id);
        insert newGroupMemberUser; 

        User lstAdminDel = new User( LastName = 'Test User Two', Email = 'testuser011@gmail.com', alias ='n_user2', Username = 'randomtestuser00111@gmail.com', emailencodingkey='UTF-8', languagelocalekey='en_US', timezonesidkey='Europe/London', localesidkey='en_US', ProfileId = profUser.Id, isActive = True);
        insert  lstAdminDel;
        GroupMember newGroupMemberAdmin = new GroupMember(GroupId = grpAdmin.Id, UserOrGroupId = lstAdminDel.Id);
        insert newGroupMemberAdmin;


        Test.startTest();
        AssignGroupBatch gBDel = new AssignGroupBatch();
        Id jobId = Database.executeBatch(gBDel);
        Test.stopTest();

    }

}
