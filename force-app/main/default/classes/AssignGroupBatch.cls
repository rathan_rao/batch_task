global with sharing class AssignGroupBatch implements Database.Batchable<SObject> 
{
    global Database.QueryLocator start( Database.BatchableContext bc)
    {
        return Database.getQueryLocator([SELECT Id, Name, Profile.Name, isActive, ProfileId FROM User WHERE isActive = True ]);
    }
    global void execute( Database.BatchableContext bc, List<User> lstUser )
    {   
       // System.debug(lstUser.size());
        List<GroupMember> lstGmNew = new List<GroupMember>();
        Map<Id, User> mapUser = new Map<Id, User >();
        Group grpUser =[SELECT Id, Name FROM Group WHERE Name ='Users'];
        Group grpAdmin =[SELECT Id, Name FROM Group WHERE Name ='Admins'];
        for( User objUser : lstUser)
        {
          mapUser.put(objUser.Id, objUser);
          if( objUser.Profile.Name == 'Standard User' )
          {
              GroupMember gmUser = new GroupMember();
              gmUser.GroupId = grpUser.Id;
              gmUser.UserOrGroupId = objUser.Id;
              lstGmNew.add(gmUser);
          }
          else if ( objUser.Profile.Name == 'System Administrator' )
          {
              GroupMember gmAdmin = new GroupMember();
              gmAdmin.GroupId = grpAdmin.Id;
              gmAdmin.UserOrGroupId = objUser.Id;
              lstGmNew.add(gmAdmin);
          }
        }
        
        //REMOVE USERS WHICH ARE NOT ASSOCIATED TO PROFILE NAMES "STANDARD USED" & "SYSTEM ADMINISTRATOR" FROM THE ASSGINED PUBLIC GROUPS 
        List<GroupMember> lstGm = [SELECT Group.Name, GroupId, UserOrGroupId FROM GroupMember ];
        List<GroupMember> lstGmRemove = new List<GroupMember>();
        Set<Id> setIdUser = new Set<Id>();
        Set<Id> setIdAdmin = new Set<Id>();
        Set<Id> setOfAdmins = new Set<Id>();
        Set<Id> setOfUsers = new Set <Id>();

        for( GroupMember objGm : lstGm )
        {
            System.debug('objGm.Group.Name   '+objGm.Group.Name +'    objGm.UserOrGroupId).Profile.Name   '+ mapUser.get(objGm.UserOrGroupId).Profile.Name + 'NAME:    '+ mapUser.get(objGm.UserOrGroupId).Name);
            if( objGm.Group.Name == 'Users')
            {
                setIdUser.add(objGm.GroupId);
                setOfUsers.add(objGm.UserOrGroupId);
                if( mapUser.get(objGm.UserOrGroupId).Profile.Name != 'Standard User')
                {
                    lstGmRemove.add(objGm);

                }
            }
                else if( objGm.Group.Name == 'Admins')
                {
                    setIdAdmin.add(objGm.GroupId);
                    setOfAdmins.add(objGm.UserOrGroupId);
                    if( mapUser.get( objGm.UserOrGroupId ).Profile.Name != 'System Administrator')
                    {
                        lstGmRemove.add(objGm);
                    }
                }

            }
            if(!lstGmNew.isEmpty())
        {
            System.debug('Group Member List is ' + lstGmNew);
            insert lstGmNew;
        }
        if( !lstGmRemove.isEmpty())
        {
            delete lstGmRemove;
        }
        }
        global void finish( Database.BatchableContext bc)
    {
        //System.System.debug('Done !!');

    }
        
    }
    

